Solr Server
===========

Maven template for Solr Server projects. 

Requirements
-----------------

* Maven 3 (thats what I tested)
* Java 1.6 (that's what I tested)

Running
----------

    cd to solr-maven-template project directory
    mvn jetty:run

*Note: By default the project is configured to run Solr out of the current project directory and keep it's core data under a `solr` subdirectory - you can change that by changing*

__* profiles/dev/config.properties *__

    solr.solr.home=./solr

Configuration
-----------------

* profiles - directory containing config files for your project 
* src/main/config - directory containing config files specific to Solr core instance (e.g. solrconfig.xml, stopwords.txt)
* src/main/resources - contains solr.xml to configure Solr server



